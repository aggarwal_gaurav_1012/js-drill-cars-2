// Function to extract years from the inventory
function getCarYears(inventory) {
    return inventory.map(car => car.car_year);
}

// Export the function
module.exports = getCarYears;