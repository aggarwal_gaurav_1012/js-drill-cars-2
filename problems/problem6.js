function filterBMWAndAudi(inventory) {
    
    // Filter the inventory to include only BMW and Audi cars
    const BMWAndAudi = inventory.filter(car => car.car_make === 'BMW' || car.car_make === 'Audi');
    return BMWAndAudi;
}

module.exports = filterBMWAndAudi;