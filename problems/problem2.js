// Function to get information of the last car in the inventory
function getLastCarInfo(inventory) {

    // Use map to extract car make and model for each car
    const carInfoArray = inventory.map(car => ({
        make: car.car_make,
        model: car.car_model
    }));

    // Use reduce to find the information of the last car
    const lastCarInfo = carInfoArray.reduce((acc, car) => car, {});

    return lastCarInfo;
}

module.exports = getLastCarInfo;