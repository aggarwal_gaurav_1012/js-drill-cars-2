// Function to find cars older than the year 2000
function findOlderCars(inventory) {
    const olderCars = inventory.filter(car => car.car_year < 2000);
    return olderCars;
}

module.exports = findOlderCars;