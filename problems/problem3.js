function sortCarModelsAlphabetically(inventory) {

    // Extract all car model names from the inventory
    const carModels = inventory.map(car => car.car_model);
    
    // Sort the car model names alphabetically
    const sortedCarModels = carModels.sort();
    
    return sortedCarModels;
}

module.exports = sortCarModelsAlphabetically;