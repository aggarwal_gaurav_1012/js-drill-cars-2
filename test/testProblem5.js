const findOlderCars = require('../problems/problem5');
const inventory = require('../data');

// Test case
const olderCars = findOlderCars(inventory);

console.log("Number of cars older than 2000:", olderCars.length);

console.log("List of older cars:", olderCars);