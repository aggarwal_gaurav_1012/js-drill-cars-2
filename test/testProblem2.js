const getLastCarInfo = require('../problems/problem2');

const inventory = require('../data');

// Test the getLastCarInfo function
const lastCarInfo = getLastCarInfo(inventory);
console.log(`Last car is a ${lastCarInfo.make} ${lastCarInfo.model}`);