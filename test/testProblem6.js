const filterBMWAndAudi = require('../problems/problem6');

const inventory = require('../data');

const BMWAndAudi = filterBMWAndAudi(inventory);

// use JSON.stringify() to show the results of the array in the console.
console.log(JSON.stringify(BMWAndAudi, null, 2));