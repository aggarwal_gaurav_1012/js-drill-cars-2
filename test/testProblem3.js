const sortCarModelsAlphabetically = require('../problems/problem3');
const inventory = require('../data');

// Test the sortCarModelsAlphabetically function
const sortedCarModels = sortCarModelsAlphabetically(inventory);

console.log(sortedCarModels); // Log the sorted car models